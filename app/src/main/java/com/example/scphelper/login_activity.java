package com.example.scphelper;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;



public class login_activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle State) {
        super.onCreate(State);
        setContentView(R.layout.activity_login_activity);

        Button SignIn = findViewById(R.id.signIn_button);


        //По клику на кнопку проверяет логин и пароль, запускает Home.class
        SignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(check_authData(v)){
                    EditText username = findViewById(R.id.Login);
                    String usernameStr = username.getText().toString();
                    go_Home(usernameStr);
                }
            }
        });
    }

    //Сохраняет сообщение о неверном логине.пароле при смене ориентации экрана
    @Override
    protected void onSaveInstanceState(Bundle State){
        TextView messageToSave = findViewById(R.id.ErrorMessage);
        String  messageStr = messageToSave.getText().toString();
        State.putString("ErrorMessage", messageStr);
        State.putInt("Color", messageToSave.getCurrentTextColor());

        super.onSaveInstanceState(State);
    }


    //Сохраняет сообщение о неверном логине.пароле при смене ориентации экрана
    @Override
    protected void onRestoreInstanceState(Bundle State){
        super.onRestoreInstanceState(State);


        TextView messageToRestore = findViewById(R.id.ErrorMessage);
        String messageStr = State.getString("ErrorMessage");
        messageToRestore.setText(messageStr);
        messageToRestore.setTextColor(State.getInt("Color"));
    }



    //Проверяет авторизационные данные и возврщает 0/1 если они неверны/верны
    public boolean check_authData(View v) {
        TextView message = findViewById(R.id.ErrorMessage);

        EditText Login = findViewById(R.id.Login);
        String LoginStr = Login.getText().toString();

        EditText Password = findViewById(R.id.Password);
        String PasswordStr = Password.getText().toString();

        String[] Logins = getResources().getStringArray(R.array.Logins);
        String[] Passwords = getResources().getStringArray(R.array.passwords);

        boolean logged_in = false;

        for (int i = 0; i < Logins.length && !logged_in; i++) {
            if (LoginStr.equals(Logins[i]) && PasswordStr.equals(Passwords[i])) {
                message.setTextColor(Color.GREEN);
                message.setText("Signing in...");
                logged_in = true;
            } else {
                message.setTextColor(Color.RED);
                message.setText("Wrong login or password!");
            }
        }
        return logged_in;
    }

    public void go_Home(String username){
        Context ctx = this;
        Intent Home = new Intent(ctx, Home.class);
        Home.putExtra("username", username);
        startActivity(Home);
        TextView message = findViewById(R.id.ErrorMessage);
        message.setText("");
    }


}


