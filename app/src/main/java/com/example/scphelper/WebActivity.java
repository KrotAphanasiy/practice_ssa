package com.example.scphelper;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        Bundle args = getIntent().getExtras();
        String url = args.get("URL").toString();
        String app_name = getResources().getString(R.string.app_name);

        String username = args.get("username").toString();
        setTitle(app_name + " User: " + username);


        WebView MyWebView = findViewById(R.id.mWebView);

        WebViewClient mWBC = new MyWebViewClient();
        MyWebView.setWebViewClient(mWBC);


        WebSettings webSettings = MyWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        MyWebView.loadUrl(url);
        MyWebView.setInitialScale(150);
    }

}
