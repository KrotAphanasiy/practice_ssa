package com.example.scphelper;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.Arrays;
import java.util.List;

public class Home extends AppCompatActivity {
    private RecyclerView SCP_List;
    private RecyclerView.LayoutManager SCP_List_LayoutManager;
    private RecyclerView.Adapter mAdapter;

    private List<String> SCP_names;
    private List<String> SCP_classes;
    private List<String> SCP_urls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        String app_name = getResources().getString(R.string.app_name);
        Bundle args = getIntent().getExtras();
        String username = args.get("username").toString();
        setTitle(app_name + " User: " + username);

        SCP_List = findViewById(R.id.SCP_List);
        SCP_List.setHasFixedSize(true);
        SCP_List_LayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        SCP_List.setLayoutManager(SCP_List_LayoutManager);


        String [] SCP_names_strs = getResources().getStringArray(R.array.SCP_names);
        String [] SCP_classes_strs = getResources().getStringArray(R.array.SCP_classes);
        String [] SCP_urls_strs = getResources().getStringArray(R.array.SCP_urls);


        SCP_names = Arrays.asList(SCP_names_strs);
        SCP_classes = Arrays.asList(SCP_classes_strs);
        SCP_urls = Arrays.asList(SCP_urls_strs);


        mAdapter = new RecyclerAdapter(SCP_names, SCP_classes, SCP_urls, username, this);
        SCP_List.setAdapter(mAdapter);



    }


}

