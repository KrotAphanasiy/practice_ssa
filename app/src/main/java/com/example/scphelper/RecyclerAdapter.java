package com.example.scphelper;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {
    private List<String> SCP_names;
    private List<String> SCP_classes;
    private List<String> URLs;
    private Context Home_ctx;
    private String username;


    public RecyclerAdapter(List<String> SCP_names_arg, List<String> SCP_class_arg, List<String> URLs_arg, String user_arg, Context ctx_arg){
        SCP_names = SCP_names_arg;
        SCP_classes = SCP_class_arg;
        URLs = URLs_arg;
        Home_ctx = ctx_arg;
        username = user_arg;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView SCP_name;
        TextView SCP_class;
        String URL;
        public MyViewHolder(View itemView) {
            super(itemView);
            SCP_name = itemView.findViewById(R.id.SCP_name);
            SCP_class = itemView.findViewById(R.id.SCP_class);

        }
    }


    @Override
    public RecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_item, parent, false);
        MyViewHolder vh = new MyViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
            holder.SCP_name.setText(SCP_names.get(position));
            holder.SCP_class.setText(SCP_classes.get(position));
            holder.URL = URLs.get(position);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Home_ctx);
                    builder.setCancelable(true)
                            .setIcon(R.drawable.scp_foundation_logo)
                            .setTitle(SCP_names.get(position))
                            .setMessage("Открыть с помоцью...")
                            .setNegativeButton("В приложении", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    go_URL_inApp(URLs.get(position));
                                }
                            })
                            .setPositiveButton("В браузере", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    go_URL_inBrowser(URLs.get(position));
                                }
                            })
                            .setNeutralButton("Отмена", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    return;
                                }
                            });
                    builder.show();
                }
            });

    }

    @Override
    public int getItemCount() {
        return SCP_names.size();
    }

    public void go_URL_inApp(String URL){
        Intent Web = new Intent(Home_ctx, WebActivity.class);
        Web.putExtra("URL", URL);
        Web.putExtra("username", username);

        Home_ctx.startActivity(Web);


    }

    public void go_URL_inBrowser(String URL){
        Uri link = Uri.parse(URL);
        Intent openLink = new Intent(Intent.ACTION_VIEW, link);
        Home_ctx.startActivity(openLink);
    }

}
